﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;

public static class DropdownPerformer
{
    public static void PerformDropdownOptions(Dropdown dropdown)
    {
        dropdown.ClearOptions();
        List<string> options = new List<string>();
        for (int i = 0; i < Enum.GetNames(typeof(ResourceType)).Length; i++)
        {
            options.Add(Enum.GetName(typeof(ResourceType), i));
        }
        dropdown.AddOptions(options);
    }
}