using System.Collections.Generic;

public class ResourcePool
{
    public Dictionary<ResourceType, int> Pool;

    public ResourcePool()
    {
        Pool = new Dictionary<ResourceType, int>()
        {
            [ResourceType.Cats] = 0,
            [ResourceType.Cream] = 0,
            [ResourceType.Cusions] = 0
        };
    }

    public void ResetPool()
    {
        Pool = new Dictionary<ResourceType, int>()
        {
            [ResourceType.Cats] = 0,
            [ResourceType.Cream] = 0,
            [ResourceType.Cusions] = 0
        };
    }
}

public enum ResourceType
{
    Cream,
    Cats,
    Cusions
}