using System;
using System.Runtime.Serialization;
using UnityEngine;

public class AddMenuController : IUIController
{
    public UISwitcher _switcher { get; set; }
    private AddMenuView _addMenuView;
    private ResourcePool _pool;

    public AddMenuController(UISwitcher switcher, AddMenuView addMenuView, ResourcePool pool)
    {
        _switcher = switcher;
        _addMenuView = addMenuView;
        _pool = pool;
    }
    public void Enter()
    {
        _addMenuView.gameObject.SetActive(true);
        _addMenuView.OnAddButtonPressed += OnAddButtonPressed;
        DropdownPerformer.PerformDropdownOptions(_addMenuView.ResourceToAdd);
    }
    
    public void Exit()
    {
        _addMenuView.gameObject.SetActive(false);
        _addMenuView.OnAddButtonPressed -= OnAddButtonPressed;
    }
    
    private void OnAddButtonPressed()
    {
        _pool.Pool[(ResourceType) _addMenuView.ResourceToAdd.value] += 
            int.Parse(_addMenuView.AddAmountInputField.text);
    }
}