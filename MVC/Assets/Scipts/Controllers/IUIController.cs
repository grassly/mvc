public interface IUIController
{
    UISwitcher _switcher { get; set; }
    void Enter();
    void Exit();
}