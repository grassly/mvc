using UnityEngine;

public class RemoveMenuController : IUIController
{
    public UISwitcher _switcher { get; set; }
    private RemoveMenuView _removeMenuView;
    private ResourcePool _pool;

    public RemoveMenuController(UISwitcher switcher, RemoveMenuView removeMenuView, ResourcePool pool)
    {
        _switcher = switcher;
        _removeMenuView = removeMenuView;
        _pool = pool;
    }
    public void Enter()
    {
        _removeMenuView.gameObject.SetActive(true);
        _removeMenuView.OnRemoveButtonPressed += OnRemoveButtonPressed;
        DropdownPerformer.PerformDropdownOptions(_removeMenuView.ResourceToRemove);
    }

    public void Exit()
    {
        _removeMenuView.gameObject.SetActive(false);
        _removeMenuView.OnRemoveButtonPressed -= OnRemoveButtonPressed;
    }
    
    private void OnRemoveButtonPressed()
    {
        if (_pool.Pool[(ResourceType) _removeMenuView.ResourceToRemove.value]
            > int.Parse(_removeMenuView.RemoveAmountInputField.text))
            _pool.Pool[(ResourceType) _removeMenuView.ResourceToRemove.value] -=
                int.Parse(_removeMenuView.RemoveAmountInputField.text);
        else _pool.Pool[(ResourceType) _removeMenuView.ResourceToRemove.value] = 0;
    }
}