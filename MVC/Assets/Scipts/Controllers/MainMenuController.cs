using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : IUIController
{
    private MainMenuView _mainMenuView;
    public UISwitcher _switcher { get; set; }
    private ResourcePool _pool;
    public MainMenuController(UISwitcher switcher, MainMenuView mainMenuView, ResourcePool resourcePool)
    {
        _switcher = switcher;
        _mainMenuView = mainMenuView;
        _pool = resourcePool;
    }

    public void Enter()
    {
        _mainMenuView.gameObject.SetActive(true);
        _mainMenuView.OnResetButtonPressed += OnResetButtonPressed;
        
        PerformResourcesPanel();
    }

    private void PerformResourcesPanel()
    {
        //при н.у. здесь должны быть не дестрой и инстанциироваание,
        //а пул уже созданных яйчеек, которые просто обновляются в этом методе
        
        for (int i = 0; i < _mainMenuView.cellRoot.childCount; i++)
        {
           GameObject.Destroy(_mainMenuView.cellRoot.GetChild(i).gameObject);
        }
        
        foreach (var item in _pool.Pool)
        {
            GameObject prefab = Resources.Load("Cell", typeof(GameObject)) as GameObject;
            GameObject cell = MonoBehaviour.Instantiate(prefab, _mainMenuView.cellRoot);
            
            CellView cellView = cell.GetComponent<CellView>();
            
            cellView.AmountText.text = item.Value.ToString();
            cellView.NameText.text = Enum.GetName(typeof(ResourceType), item.Key);
        }
    }
    
    public void Exit()
    {
        _mainMenuView.gameObject.SetActive(false);
        _mainMenuView.OnResetButtonPressed -= OnResetButtonPressed;
    }

    private void OnResetButtonPressed()
    {
        /*foreach (var item in _pool.Pool)
        {
            _pool.Pool[item.Key] = 0;
        }*/
        _pool.ResetPool();
        PerformResourcesPanel();
    }
}