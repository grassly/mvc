﻿using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class UISwitcher
{
    private Dictionary<Type, IUIController> _uiStates;
    private IUIController _currentState;

    private Button _showMainMenuButton;
    private Button _showAddMenuButton;
    private Button _showRemoveMenuButton;
    public UISwitcher(MainMenuView mainMenuView, AddMenuView addMenuView, RemoveMenuView removeMenuView,
        Button showMainMenu, Button showAddMenu, Button showRemoveMenu,
        ResourcePool pool)
    {
        _uiStates = new Dictionary<Type, IUIController>()
        {
            [typeof(MainMenuController)] = new MainMenuController(this, mainMenuView, pool),
            [typeof(AddMenuController)] = new AddMenuController(this, addMenuView, pool),
            [typeof(RemoveMenuController)] = new RemoveMenuController(this, removeMenuView, pool)
        };

        _showMainMenuButton = showMainMenu;
        _showAddMenuButton = showAddMenu;
        _showRemoveMenuButton = showRemoveMenu;

        BindButtons();
        ChangeStateToMainMenu();
    }

    private void BindButtons()
    {
        _showMainMenuButton.onClick.AddListener(ChangeStateToMainMenu);
        _showAddMenuButton.onClick.AddListener(ChangeStateToAddMenu);
        _showRemoveMenuButton.onClick.AddListener(ChangeStateToRemoveMenu);
    }

    private void ChangeStateToMainMenu() => 
        ChangeState(typeof(MainMenuController));
    
    private void ChangeStateToAddMenu() => 
        ChangeState(typeof(AddMenuController));
    
    private void ChangeStateToRemoveMenu() => 
        ChangeState(typeof(RemoveMenuController));

    public void ChangeState(Type stateType)
    {
        _currentState?.Exit();
        _currentState = _uiStates[stateType];
        _currentState?.Enter();
    }

}