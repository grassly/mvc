using System;
using UnityEngine;
using UnityEngine.UI;

public class AddMenuView : MonoBehaviour
{
    public Button addButton;
    public InputField AddAmountInputField;
    public Dropdown ResourceToAdd;
    
    public Action OnAddButtonPressed;
    public void InvokeContextAction() => OnAddButtonPressed?.Invoke();
}