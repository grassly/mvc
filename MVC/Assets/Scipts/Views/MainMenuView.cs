﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuView : MonoBehaviour
{
    public Button resetButton;
    public Transform cellRoot;
    
    public Action OnResetButtonPressed;
    public void InvokeContextAction() => OnResetButtonPressed?.Invoke();
}