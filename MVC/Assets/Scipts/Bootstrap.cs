using System;
using UnityEngine;
using UnityEngine.UI;

public class Bootstrap : MonoBehaviour
{
    [SerializeField] private MainMenuView mainMenuView;
    [SerializeField] private AddMenuView addMenuView;
    [SerializeField] private RemoveMenuView removeMenuView;

    [SerializeField] private Button ShowMainMenuButton;
    [SerializeField] private Button ShowAddMenuButton;
    [SerializeField] private Button ShowRemoveMenuButton;
    private void Awake()
    {
        ResourcePool pool = new ResourcePool();
        UISwitcher uiSwitcher = new UISwitcher(mainMenuView, addMenuView, removeMenuView,
            ShowMainMenuButton, ShowAddMenuButton, ShowRemoveMenuButton,
            pool);
    }
}